<?php

namespace App\Http\Controllers;
use App\Models\Story;
use Illuminate\Http\Request;

class PostLikeController extends Controller
{   

    public function __construct(){
        $this->middleware(['auth']);
    }

    public function store(Story $story, Request $request){
        
        if ($story->likedBy($request->user())){
            return response(null, 409);
        }

        $story->likes()->create([
            'user_id' => $request->user()->id,
        ]);
        return back();
    }
    
    public function destroy(Story $story, Request $request){
        $request->user()->likes()->where('story_id', $story->id)->delete();
        
        return back();
    }

}
