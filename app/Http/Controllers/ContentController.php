<?php

namespace App\Http\Controllers;
use App\Models\Story;
use App\Models\User;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    public function index(){
        $stories = Story::paginate(10);
        return view('posts.contentTable', [
            'stories' => $stories
        ]);
    }

    public function sortedDesc(){
        $stories = Story::latest()->paginate(10);
        return view('posts.contentTableDesc', [
            'stories' => $stories
        ]);
    }

    public function sortedByLikes(User $user){

        $stories = Story::withCount('likes')->orderByDesc('likes_count')->get();
        return view('posts.contentTable', [
            'stories' => $stories
        ]);
    }

    public function displayTopStories(User $user){
      
        $stories = Story::withCount('likes')->orderByDesc('likes_count')->get();
        return view('posts.topStories', [
            'stories' => $stories
        ]);
    }

    public function displayTopAuthors(User $users){
        $users = User::withCount('receivedLikes')->orderByDesc('received_likes_count')->get();
       
       // $stories = User::withCount('likes')->orderByDesc('likes_count')->get();
        return view('posts.hallOfFame', [
            'users' => $users
        ]);
    }

    public function detail($id){
        $story = Story::where('id', $id)->first();
        return view('detail', ['story' => $story]);
    }
}
