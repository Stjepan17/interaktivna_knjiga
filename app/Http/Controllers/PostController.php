<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Story;

class PostController extends Controller
{

    public function __construct(){
        $this->middleware(['auth']);
    }

    public function index(){
        $stories = Story::with('user', 'likes')->paginate(1);
        return view('posts.index', [
            'stories' => $stories
        ]);
    }

    public function store(Request $request){
        $this->validate($request, [
            'body' => 'required',
            'title'=> 'required'
        ]);

        $story = $request->user()->stories()->create([
            'body' =>$request->body,
            'title' => $request->title
        ]);

        
        return back();
    }
    public function createPage(){
        return view('posts.createPage');
    }

    public function destroy(Story $story){
        $this->authorize('delete', $story);
        $story->delete();
        return back();
    }
}
