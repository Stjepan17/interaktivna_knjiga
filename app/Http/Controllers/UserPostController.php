<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
class UserPostController extends Controller
{
    public function index(User $user){
        $stories = $user->stories()->with(['user', 'likes'])->paginate(20);
        return view('users.posts.index', [
            'user' => $user,
            'stories' => $stories
        ]);
    }
}
