@extends('layouts.app')


@section('content')

    <div class="flex justify-center">
        <div class="w-1/2 bg-white p-6 rounded-lg">
            
                <div>
                    <h1 class="text-center font-bold mb-10 text-3xl">TOP STORIES</h1>
                      
                </div>
            <?php
                $i = 0;
            ?>
            @if($stories->count())
            <div class="mb-4 mt-4 flex flex-col space-y-4">
                

                @foreach ($stories as $story)
                
                <div class="mb-4">
                    <a href="{{ route('users.posts', $story->user) }}" class="font-bold">{{ $story->user->username }}</a>
                    <span class="text-gray-600 text-sm">{{ $story->created_at->diffForHumans() }}</span>
                    <h1 class="mb-4">{{ $story->title }}</h1>
                    {!! $story->body !!}

                   
                    <div class="flex itmes-center">
                        @auth
                        @if(!$story->likedBy(auth()->user()))
                        <form action="{{ route('posts.likes', $story) }}" method="post" class="mr-1">
                            @csrf
                            <button type="submit" class="text-blue-500">Like</button>
                        </form>
                        @else
                        <form action="{{ route('posts.likes', $story) }}" method="post" class="mr-1">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="text-blue-500">Unlike</button>
                        </form>
                        @endif

                        

                        @endauth
                        <span>{{ $story->likes->count() }} @if ($story->likes->count()==1)
                            like
                            @else likes
                        @endif</span>
                    </div>
                </div>
                <?php
                        $i++;
                        if($i == 3) {
                            break; // because we don't want to continue the loop
                        }
                    
                    ?>
                @endforeach
                
            </div>
                {{-- $stories->links() --}}
            @else
                <p>There are no stories</p>
            @endif
        </div>
    </div>
@endsection