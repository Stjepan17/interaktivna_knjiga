@extends('layouts.app')


@section('content')

    <div class="flex justify-center">
        <div class="w-1/2 bg-white p-6 rounded-lg">
            
                <div>
                    <h1 class="text-center font-bold mb-10 text-3xl">TOP AUTHORS</h1>
                      
                </div>
            <?php
                $i = 0;
            ?>
     
                

                @isset($users)
                
                <div class="mb-4 mt-4 flex flex-col space-y-4">
                    @forelse ($users as $user)
                        <div class="bg-blue-400 rounded-lg p-3 justify-between text-center">
                            <p class="font-bold">{{ $user->name }} received {{ $user->received_likes_count }} {{ Str::plural('like', $user->received_likes_count) }}</p>   
                        </div>
                        @empty
                        <p>There are no authors</p>
                    @endforelse
                </div>
                    {{-- $stories->links() --}}
          
                    
                    @endisset
               
              
                
            </div>
               
            
        </div>
    </div>
@endsection