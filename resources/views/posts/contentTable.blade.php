@extends('layouts.app')


@section('content')

    <div class="flex justify-center">
        <div class="w-1/3 bg-white p-6 rounded-lg">
            
                <div>
                    <h1 class="text-center font-bold mb-4 text-3xl">Content table</h1>
                    <div class="flex font-bold justify-around">
                        
                        <a href="{{ route('contentTableDesc') }}" class="bg-blue-500 py-2 text-gray-100 px-7 rounded-full">Sort by newest</a>
                        <a href="{{ route('contentTableLiked') }}" class="bg-blue-500 py-2 text-gray-100 px-7 rounded-full">Sort by likes</a>
                    </div>  
                </div>
            @if($stories->count())
            <div class="mb-4 mt-4 flex flex-col space-y-4">
                @foreach ($stories as $story)
                    <div class="bg-blue-400 rounded-lg p-3 justify-between text-center">
                        <a href="{{ route('story.detail', $story->id) }}" class="font-bold">{{ $story->title }}</a>   
                    </div>
       
                @endforeach
            </div>
                {{-- $stories->links() --}}
            @else
                <p>There are no stories</p>
            @endif
        </div>
    </div>
@endsection