@extends('layouts.app')


@section('content')
<script src="https://cdn.ckeditor.com/ckeditor5/29.2.0/classic/ckeditor.js"></script>
    <div class="flex justify-center">
        <div class="w-8/12 bg-white p-6 rounded-lg">
            

            @if($stories->count())
                @foreach ($stories as $story)
                    <div class="mb-4">
                        <a href="{{ route('users.posts', $story->user) }}" class="font-bold">{{ $story->user->username }}</a>
                        <span class="text-gray-600 text-sm">{{ $story->created_at->diffForHumans() }}</span>
                        <h1 class="mb-4">{{ $story->title }}</h1>
                        <textarea class="mb-2" id="note">{{ $story->body }}</textarea>

                        @can('delete', $story)
                            <form action="{{ route('posts.destroy', $story) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="text-blue-500">Delete</button>
                            </form>
                        @endcan

                        <div class="flex itmes-center">
                            @auth
                            @if(!$story->likedBy(auth()->user()))
                            <form action="{{ route('posts.likes', $story) }}" method="post" class="mr-1">
                                @csrf
                                <button type="submit" class="text-blue-500">Like</button>
                            </form>
                            @else
                            <form action="{{ route('posts.likes', $story) }}" method="post" class="mr-1">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="text-blue-500">Unlike</button>
                            </form>
                            @endif

                            

                            @endauth
                            <span>{{ $story->likes->count() }} @if ($story->likes->count()==1)
                                like
                                @else likes
                            @endif</span>
                        </div>
                    </div>
                @endforeach
                {{ $stories->links() }}
            @else
                <p>There are no posts</p>
            @endif
        </div>
        <script>
            
            ClassicEditor.create( document.querySelector('#note' ) )
     .then(editor => { 
          console.log( editor ); 
          editor.isReadOnly = true; // make the editor read-only right after initialization
     } ) .catch( error => { 
          console.error( error ); 
     } );
        </script>
    </div>
@endsection