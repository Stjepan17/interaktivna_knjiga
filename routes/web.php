<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\PostLikeController;
use App\Http\Controllers\ContentController;
use App\Http\Controllers\UserPostController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function(){
    return view('home');
})->name('home');

Route::get('/dashboard', [DashboardController::class,'index'])->name('dashboard');
Route::get('/content', [ContentController::class,'index'])->name('contentTable');

Route::get('/content/desc', [ContentController::class,'sortedDesc'])->name('contentTableDesc');
Route::get('/content/liked', [ContentController::class,'sortedByLikes'])->name('contentTableLiked');

Route::get('/topStories', [ContentController::class,'displayTopStories'])->name('topStories');
Route::get('/hallOfFame', [ContentController::class,'displayTopAuthors'])->name('topAuthors');

Route::get('/story/{id}', [ContentController::class, 'detail'])->name('story.detail');

Route::get('/users/{user}/posts', [UserPostController::class,'index'])->name('users.posts');

Route::post('/logout', [LogoutController::class,'store'])->name('logout');

Route::get('/login', [LoginController::class,'index'])->name('login');
Route::post('/login', [LoginController::class,'store']);

Route::get('/register', [RegisterController::class,'index'])->name('register');
Route::post('/register', [RegisterController::class,'store']);


Route::get('/posts', [PostController::class, 'index'])->name('posts');
Route::post('/posts', [PostController::class, 'store'])->middleware('auth');
Route::delete('/posts/{story}', [PostController::class, 'destroy'])->name('posts.destroy');

Route::post('/posts/{story}/likes', [PostLikeController::class, 'store'])->name('posts.likes');
Route::delete('/posts/{story}/likes', [PostLikeController::class, 'destroy'])->name('posts.likes');
Route::get('/createPage', [PostController::class, 'createPage'])->name('createPage');